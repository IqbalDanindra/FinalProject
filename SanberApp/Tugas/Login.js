import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button } from "react-native";
import { State } from "react-native-gesture-handler";

export default function Login({ navigation }) {
const [username, setUsername] = useState("");
const [password, setPassword] = useState("");
const [isError, setIsError] = useState(false);

  const submit = () => {
    console.log({username, password})
   if (password == '12345678'){
     setIsError(false)
     navigation.navigate('Home', {username})
   } else {
     setIsError(true)
   }
    //tuliskan coding disini 
    //? #Soal No. 1 (15poin) -- LoginScreen.js -- Function LoginScreen
    //? Buatlah sebuah fungsi untuk berpindah halaman hanya jika password yang di input bernilai '12345678' 
    //? dan selain itu, maka akan mengubah state isError menjadi true dan tidak dapat berpindah halaman.

    //? #SoalTambahan (+ 5 poin): kirimkan params dengan key => userName dan value => this.state.userName ke halaman Home, 
    //? dan tampilkan userName tersebut di halaman Home setelah teks "Hai," -->
    //end coding
  };
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 20, fontWeight: "bold" }}>TOKO ONLINEKU</Text>
      <Image
        style={{ height: 150, width: 150 }}
        source={require("./assets/tokoku.jpg")}
      />
      <View style={styles.container2}>
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
            backgroundColor: 'white',
          }}
          placeholder="Masukan Username"
          value={username}
          onChangeText={(value)=>setUsername(value)}
        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
            backgroundColor: 'white',
          }}
          placeholder="Masukan Password"
          value={password}
          onChangeText={(value)=>setPassword(value)}
        />
        <Button onPress={submit} title="Login" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  container2: {
    flex: 0.5,
    borderWidth: 1,
    paddingVertical: 10,
            borderRadius: 5,
            width: 350,
            marginBottom: 0,
            paddingHorizontal: 10,
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",
  },
});
